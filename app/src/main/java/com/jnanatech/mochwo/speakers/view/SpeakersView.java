package com.jnanatech.mochwo.speakers.view;

import com.jnanatech.mochwo.speakers.model.SpeakerModel;

import java.util.ArrayList;

public interface SpeakersView {

    void getSpeaker(ArrayList<SpeakerModel> speakers);
}
