package com.jnanatech.mochwo.schedule.presenter;

public interface DayOnePresenter {

    public void setDayOneEvents();
    public void setDayTwoEvents();
}
