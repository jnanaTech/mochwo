package com.jnanatech.mochwo.aboutUs.presenter;

public interface AboutUsPresenter {

    void setOrganizingCommiteeMembers();
    void setScientificCommiteeMembers();
}
