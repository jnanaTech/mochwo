package com.jnanatech.mochwo.utils;

public class Constants {
    public static final int phonePermissionConstant = 2166;
    public static final String eventConstant = "eventDetailConstant";
    public static final String scheduleConstant = "scheduleDetailConstant";
    public static final String dateFormatDefault = "yyyy-MM-dd hh:mm:ss";
    public static final String speakerConstant = "speakerDetail";
    public static final String sponsersConstant = "speakerDetail";
    public static final String speakerImageUrlConstant = "speakerImageUrl";
    public static final String URLSpeakersAPI = "https://conference.kias.org.np/wp-json/wp/v2/speaker";
    public static final String URLSponserAPI = "https://conference.kias.org.np/wp-json/wp/v2/pages/19";
    public static final String URLSchedule1 = "https://conference.kias.org.np/wp-json/wp/v2/pages/4493";
    public static final String URLSchedule2 = "https://conference.kias.org.np/wp-json/wp/v2/pages/4495";
    public static final String URLUpdates = "https://conference.kias.org.np/wp-json/wp/v2/pages/4622";
    public static final String URLNEWS = "https://conference.kias.org.np/wp-json/wp/v2/posts";
    public static final String speakerDatabaseName = "speakers.realm";
    public static final String speakerSizeConstant = "speakerSizeConstant";
    public static final String notificationCount = "notificationCount";
    public static final String speakerSharedPrefConstant = "speakerSharedPrefDataConstant";
    public static final String bookmarkSharedPrefConstant = "speakerSharedPrefDataConstant";
    public static final String notificationActivityDataConstanst = "notificationActivityDataConstanst";
    public static final int NotificationActivityConstant = 123;

    public static final String scheduleStartTimeConstant = "scheduleStartTimeConstant";
    public static final String scheduleEndTimeConstant = "scheduleEndTimeConstant";
    public static final String scheduleTopicConstant = "scheduleTopicConstant";
    public static final String scheduleDescriptionConstant = "scheduleDescriptionConstant";
    public static final String scheduleKeywordsConstant = "scheduleKeywordsConstant";
    public static final String scheduleSpeakersConstant = "scheduleSpeakersConstant";
    public static final String scheduleSpeakersDetailConstant = "scheduleSpeakersDetailConstant";
    public static final String scheduleIDConstant = "scheduleIDConstant";
    public static final String scheduleSessionConstant = "scheduleSessionConstant";
    public static final String scheduleScheduleConstant = "scheduleScheduleConstant";
    public static final String scheduleBookmarkConstant = "scheduleBookmarkConstant";
    public static final String scheduleChairPersonConstant = "scheduleChairPersonConstant";
    public static final String scheduleChairPersonDetailConstant = "scheduleChairPersonDetailConstant";

    public static final String webUrlConstant = "webUrlConstant";
    public static final String toolbarConstant = "toolbarConstant";
    public static final String kiasImageURL = "http://conference.kias.org.np/wp-content/uploads/2019/09/KIAS.jpg";
}
