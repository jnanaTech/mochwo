package com.jnanatech.mochwo.schedule.view;

import com.jnanatech.mochwo.schedule.model.EventModel;

import java.util.ArrayList;

public interface DayOneView {

    public void getDayOneEvents(ArrayList<EventModel> dayOneEvents);
}
